import {
  Damage,
  AtkStrategy,
  DefStrategy,
  ShipInfo,
  AircraftInfo,
  ships,
  aircrafts,
} from '../data';

function isShipInfo(info: ShipInfo | AircraftInfo): info is ShipInfo {
  return (<ShipInfo>info).curHealth !== undefined;
}

export class FleetUnit {
  public amount: number;
  public damages: Damage[];
  public health: number;
  public maxHealth: number;
  public speed: number;
  public resist: number[];
  public atkStrategy: AtkStrategy;
  public defStrategy: DefStrategy;

  constructor(flagship: ShipInfo);
  constructor(aircrafts: AircraftInfo);
  constructor(info: ShipInfo | AircraftInfo) {
    if (isShipInfo(info)) {
      // Flagship
      this.amount = 1;
      this.damages = [];
      this.health = info.curHealth;
      this.maxHealth = ships[info.id].health(info.level);
      this.speed = ships[info.id].speed(info.level);
      this.resist = [...ships[info.id].resist];
      this.atkStrategy = info.atkStrategy;
      this.defStrategy = info.defStrategy;
    } else {
      // Aircrafts
      this.amount = info.amount;
      this.damages = [aircrafts[info.id].damage];
      this.health = aircrafts[info.id].health;
      this.maxHealth = aircrafts[info.id].health;
      this.speed = aircrafts[info.id].speed;
      this.resist = [...aircrafts[info.id].resist];
      this.atkStrategy = info.atkStrategy;
      this.defStrategy = info.defStrategy;
    }
  }

  get totalHealth(): number {
    return (this.amount - 1) * this.maxHealth + this.health;
  }

  get destroyed(): boolean {
    return this.amount === 0
  }

  escaped(atkSpeed: number): boolean {
    return this.defStrategy === DefStrategy.Avoid && this.speed > atkSpeed;
  }

  /**
   * let the unit take the damage and returns the damage taken
   * @returns {number} Real damage taken
   */
  damaged(damage: Damage): number {
    const realDmg: number = Math.ceil(damage.value / this.resist[damage.type]);
    if (realDmg >= this.totalHealth) {
      const output: number = this.totalHealth;
      this.amount = 0;
      this.health = 0;
      return output
    }
    if (realDmg < this.health) {
      this.health -= realDmg;
    } else {
      this.amount = Math.ceil((this.totalHealth - realDmg) / this.maxHealth);
      this.health = (this.totalHealth - realDmg) % this.maxHealth;
    }
    return realDmg;
  }
}
