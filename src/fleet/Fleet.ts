import {
  AtkStrategy,
  Target,
  Buffs,
  ShipInfo,
  AircraftInfo,
  ModInfo,
  Damage,
  DamageType,
  mods,
} from '../data';
import { FleetUnit } from './FleetUnit';

interface Shields {
  ofAll: number;
  ofFlagship: number;
  ofAircrafts: number;
}

export class Fleet {
  public units: FleetUnit[] = [];

  // Shields & armor
  public shield: Shields = {
    ofAll: 0,
    ofFlagship: 0,
    ofAircrafts: 0,
  };
  public flagshipArmor: number = 0;

  // Specials
  public salvage: number = 0;

  constructor(
    shipInfo: ShipInfo,
    aircraftInfo: AircraftInfo[],
    modInfo: ModInfo[],
  ) {
    this.units.push(new FleetUnit(shipInfo));
    this.units.push(...aircraftInfo.map(info => new FleetUnit(info)));

    // Variables for buffs
    const shipWeapons: Damage[] = [];
    const shipBuffs: Buffs = {
      atk: 1,
      def: 1,
      speed: 1,
      damage: [0, 0, 0],
      resist: [0, 0, 0],
    };
    const aircraftBuffs: Buffs = {
      atk: 1,
      def: 1,
      speed: 1,
      damage: [0, 0, 0],
      resist: [0, 0, 0],
    };

    /**
     * Compute Modules' buffs
     */
    for (let i = 0; i < modInfo.length; i++) {
      const mod = mods[modInfo[i].id];
      const level = modInfo[i].level;
      if (mod.damage) {
        // Weapon
        shipWeapons.push(mod.damage(level));
      }
      if (mod.effect) {
        // Special
        if (mod.id === 400) this.salvage += mod.effect(level);
      }
      if (mod.target !== undefined) {
        // Buff
        if (mod.target === Target.Both && mod.shield) {
          // Global shield
          this.shield.ofAll += mod.shield(level);
        }
        if (mod.target === Target.Both || mod.target === Target.Ship) {
          // Bonus to flagship
          if (mod.target === Target.Ship) {
            if (mod.armor) this.flagshipArmor += mod.armor(level);
            if (mod.shield) this.shield.ofFlagship += mod.shield(level);
          }
          if (mod.atkBuff) shipBuffs.atk += mod.atkBuff(level);
          if (mod.defBuff) shipBuffs.def += mod.defBuff(level);
          if (mod.speedBuff) shipBuffs.speed += mod.speedBuff(level);
          if (mod.damageBuff) {
            const [kinetic, thermal, explosive] = mod.damageBuff(level);
            shipBuffs.damage[0] += kinetic;
            shipBuffs.damage[1] += thermal;
            shipBuffs.damage[2] += explosive;
          }
          if (mod.resistBuff) {
            const [kinetic, thermal, explosive] = mod.resistBuff(level);
            shipBuffs.resist[0] += kinetic;
            shipBuffs.resist[1] += thermal;
            shipBuffs.resist[2] += explosive;
          }
        }
        if (mod.target === Target.Both || mod.target === Target.Aircraft) {
          // Bonus to aircrafts
          if (mod.target === Target.Aircraft) {
            if (mod.shield) this.shield.ofAircrafts += mod.shield(level);
          }
          if (mod.atkBuff) aircraftBuffs.atk += mod.atkBuff(level);
          if (mod.defBuff) aircraftBuffs.def += mod.defBuff(level);
          if (mod.speedBuff) aircraftBuffs.speed += mod.speedBuff(level);
          if (mod.damageBuff) {
            const [kinetic, thermal, explosive] = mod.damageBuff(level);
            aircraftBuffs.damage[0] += kinetic;
            aircraftBuffs.damage[1] += thermal;
            aircraftBuffs.damage[2] += explosive;
          }
          if (mod.resistBuff) {
            const [kinetic, thermal, explosive] = mod.resistBuff(level);
            aircraftBuffs.resist[0] += kinetic;
            aircraftBuffs.resist[1] += thermal;
            aircraftBuffs.resist[2] += explosive;
          }
        }
      }
    }

    /**
     * Apply bonus
     */

    // Add flaghip weapon to the unit
    for (let i = 0; i < shipWeapons.length; i++) {
      shipWeapons[i].value *=
        shipBuffs.atk + shipBuffs.damage[shipWeapons[i].type];
    }
    this.units[0].damages = shipWeapons;

    // Ship resistance
    for (let i = 0; i < this.units[0].resist.length; i++) {
      this.units[0].resist[i] += shipBuffs.def + shipBuffs.resist[i];
    }
    // Ship speed
    this.units[0].speed *= shipBuffs.speed;

    // Aircrafts (i = 1 to omit flagship)
    for (let i = 1; i < this.units.length; i++) {
      // Attack
      this.units[i].damages[0].value *=
        aircraftBuffs.atk + aircraftBuffs.damage[this.units[i].damages[0].type];
      // Resistance
      for (let j = 0; j < this.units[i].resist.length; j++) {
        this.units[i].resist[j] += aircraftBuffs.def + shipBuffs.resist[j];
      }
      // Speed
      this.units[i].speed *= aircraftBuffs.speed;
    }
  }

  getPriorityList(strategy: AtkStrategy, damageType: DamageType): number[] {
    switch (strategy) {
      case AtkStrategy.LessHealth: {
        const totalHealth: number[][] = [];
        for (let i = 0; i < this.units.length; i++) {
          const elem = this.units[i];
          totalHealth[i] = [
            i,
            elem.maxHealth * (elem.amount - 1) + elem.health,
          ];
        }
        totalHealth.sort((a, b) => a[1] - b[1]);
        return totalHealth.map(x => x[0]);
      }
      case AtkStrategy.MoreAttack: {
        const totalAttack: number[][] = [];
        for (let i = 0; i < this.units.length; i++) {
          const elem = this.units[i];
          totalAttack[i] = [i, elem.damages[0].value];
        }
        totalAttack.sort((a, b) => b[1] - a[1]);
        return totalAttack.map(x => x[0]);
      }
      case AtkStrategy.LessResist: {
        const resists: number[][] = [];
        for (let i = 0; i < this.units.length; i++) {
          const elem = this.units[i];
          resists[i] = [i, elem.resist[damageType]];
        }
        resists.sort((a, b) => a[1] - b[1]);
        return resists.map(x => x[0]);
      }
      case AtkStrategy.Flagship:
      default: {
        const array: number[] = [];
        for (let i = 0; i < this.units.length; i++) {
          array.push(i);
        }
        return array;
      }
    }
  }

  get flagship(): FleetUnit {
    return this.units[0];
  }

  /**
   * Apply damage to the fleet and return if the fleet is defeated
   * @returns {boolean} Is defeated
   */
  attackedBy(
    damage: Damage,
    atkSpeed: number,
    atkStrategy: AtkStrategy,
  ): boolean {
    if (this.shielded('ofAll', damage)) return false;
    const attackOrder = this.getPriorityList(atkStrategy, damage.type);
    let unitIndex: number = -1;
    for (let i = 0; i < attackOrder.length; i++) {
      const index = attackOrder[i];
      if (!this.units[index].escaped(atkSpeed)) {
        unitIndex = index;
        break;
      }
    }
    if (unitIndex !== -1) {
      if (unitIndex === 0 && this.shielded('ofFlagship', damage)) {
        return false;
      } else if (unitIndex !== 0 && this.shielded('ofAircrafts', damage)) {
        return false;
      }
      // Something got hit!
      this.units[unitIndex].damaged(damage);
    }
    if (this.units[unitIndex].destroyed) {
      this.flagship.health += this.salvage * this.flagship.maxHealth;
      // If that thing is destroyed, remove it
      this.units.splice(unitIndex, 1);
      // If it is a flagship, fleet defeated
      if (unitIndex === 0) return true;
    }
    return false;
  }

  private shielded(
    by: 'ofAll' | 'ofFlagship' | 'ofAircrafts',
    damage: Damage,
  ): boolean {
    if (this.shield[by] > 0) {
      const realDmg: number = Math.ceil(damage.value / [3, 0, 4][damage.type]);
      if (realDmg > this.shield[by]) {
        this.shield[by] = 0;
      } else {
        this.shield[by] -= realDmg;
      }
      return true;
    } else {
      // No more shield
      return false;
    }
  }
}
