import { shipInfo, aircraftInfo, modInfo, modInfoWithoutShield } from './data';
import { Fleet } from '../Fleet';
import { getCombatResult } from '../getCombatResult';
import { Combat } from '../Combat';

describe('Combat', () => {
  test('should give the same result as getCombatResult', () => {
    const fleet1A = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleet1B = new Fleet(shipInfo, aircraftInfo, modInfo);
    getCombatResult(fleet1A, fleet1B);

    const fleet2A = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleet2B = new Fleet(shipInfo, aircraftInfo, modInfo);
    const combat = new Combat(fleet2A, fleet2B);
    let repeat = true;
    while (repeat) {
      const out = combat.nextTurn();
      repeat = out === undefined;
    }
    expect(fleet1A).toEqual(fleet2A);
    expect(fleet1B).toEqual(fleet2B);
  });
  test('fleet with shield should win', () => {
    const fleetA = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleetB = new Fleet(shipInfo, aircraftInfo, modInfoWithoutShield);
    expect(getCombatResult(fleetA, fleetB)).toBe(true);
  });
  test('defensive fleet should win if equal build', () => {
    const fleetA = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleetB = new Fleet(shipInfo, aircraftInfo, modInfo);
    expect(getCombatResult(fleetA, fleetB)).toBe(false);
  });
});
