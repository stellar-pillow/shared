import { ships, mods } from '../../data';
import { Fleet } from '../Fleet';
import { shipInfo, aircraftInfo, modInfo } from './data';

describe('Fleet', () => {
  const fleet = new Fleet(shipInfo, aircraftInfo, modInfo);
  describe('flagship', () => {
    test('should have correct resistance set', () => {
      expect(fleet.flagship.resist[0]).toBe(1 + ships[0].resist[0]);
      expect(fleet.flagship.resist[1]).toBe(1 + ships[0].resist[1]);
      expect(fleet.flagship.resist[2]).toBe(1 + ships[0].resist[2]);
    });
    test('should have correct speed set', () => {
      expect(fleet.flagship.speed).toBe(
        ships[0].speed(1) *
          (1 + (mods[300].speedBuff ? mods[300].speedBuff(5) : 0)),
      );
    });
    test('should have the effect of alpha shield', () => {
      expect(fleet.shield.ofFlagship).toBe(
        mods[200].shield ? mods[200].shield(7) : 0,
      );
    });
  });
});
