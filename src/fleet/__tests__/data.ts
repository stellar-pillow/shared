import {
  ShipInfo,
  AtkStrategy,
  DefStrategy,
  AircraftInfo,
  ModInfo,
  ships,
} from '../../data/';

export const shipInfo: ShipInfo = {
  id: 0,
  level: 1,
  curHealth: ships[0].health(1),
  atkStrategy: AtkStrategy.Flagship,
  defStrategy: DefStrategy.Avoid,
};
export const aircraftInfo: AircraftInfo[] = [
  {
    id: 0,
    amount: 20,
    atkStrategy: AtkStrategy.LessResist,
    defStrategy: DefStrategy.Normal,
  },
  {
    id: 2,
    amount: 2,
    atkStrategy: AtkStrategy.MoreAttack,
    defStrategy: DefStrategy.Normal,
  },
  {
    id: 3,
    amount: 1,
    atkStrategy: AtkStrategy.Flagship,
    defStrategy: DefStrategy.Normal,
  },
];
export const modInfo: ModInfo[] = [
  {
    id: 100,
    level: 3,
  },
  {
    id: 200,
    level: 7,
  },
  {
    id: 300,
    level: 5,
  },
];

export const modInfoWithoutShield: ModInfo[] = [
  {
    id: 100,
    level: 3,
  },
  {
    id: 300,
    level: 5,
  },
];
