import { Fleet } from './Fleet';

export class Combat {
  private _isAttackersTurn: boolean;
  public get isAttackersTurn(): boolean {
    return this._isAttackersTurn;
  }

  constructor(public attacker: Fleet, public defender: Fleet) {
    this._isAttackersTurn = attacker.flagship.speed > defender.flagship.speed;
  }

  nextTurn(): boolean | undefined {
    const [atkFleet, defFleet] = this.isAttackersTurn
      ? [this.attacker, this.defender]
      : [this.defender, this.attacker];
    for (let i = 0; i < atkFleet.units.length; i++) {
      const elem = atkFleet.units[i];
      for (let j = 0; j < elem.damages.length; j++) {
        if (
          defFleet.attackedBy(elem.damages[j], elem.speed, elem.atkStrategy)
        ) {
          return this._isAttackersTurn;
        }
      }
    }
    this._isAttackersTurn = !this._isAttackersTurn;
    return undefined;
  }
}
