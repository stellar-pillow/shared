import { DamageType, Target, Damage } from './common';

interface Mod {
  id: number;
  label: string;
  desc: string;
  size: number;
  target?: Target;
  damage?: (level: number) => Damage;
  shield?: (level: number) => number;
  armor?: (level: number) => number;
  atkBuff?: (level: number) => number;
  defBuff?: (level: number) => number;
  speedBuff?: (level: number) => number;
  damageBuff?: (level: number) => number[];
  resistBuff?: (level: number) => number[];
  effect?: (level: number) => number;
}

export const preMods: Mod[] = [
  {
    id: 100,
    label: '质量投射器',
    desc: '利用电磁加速发射炮弹',
    size: 1,
    damage: level => ({ value: 90 + level * 10, type: DamageType.Kinetic }),
  },
  {
    id: 200,
    label: '阿尔法护盾',
    desc: '保护星舰',
    size: 1,
    shield: level => 500 + level * 80,
    armor: () => 0,
    target: Target.Ship,
  },
  {
    id: 300,
    label: '外挂引擎',
    desc: '速度变快啦',
    size: 1,
    speedBuff: level => level * 0.1,
    target: Target.Ship,
  },
  {
    id: 400,
    label: '回收',
    desc: '每当一队飞机被击毁后恢复星舰装甲',
    size: 1,
    effect: level => 0.06 + level * 0.02,
  },
];

const mods: Mod[] = [];
for (let i = 0; i < preMods.length; i++) {
  mods[preMods[i].id] = preMods[i];
}

export { mods };
