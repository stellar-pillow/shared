export interface Cost {
  metal: number;
  crystal: number;
  pop: number;
  time: number;
}

export enum DamageType {
  Kinetic,
  Thermal,
  Explosive,
}

export enum Maker {
  Union,
  Empire,
  Republic,
  State,
  Group,
}

export enum AtkStrategy {
  Flagship,
  LessHealth,
  MoreAttack,
  LessResist,
}

export enum DefStrategy {
  Tank,
  Normal,
  Avoid,
}

export enum Target {
  Aircraft,
  Ship,
  Both,
}

export interface Buffs {
  atk: number;
  def: number;
  speed: number;
  damage: [number, number, number];
  resist: [number, number, number];
}

export interface ModInfo {
  id: number;
  level: number;
}

export interface ShipInfo {
  id: number;
  level: number;
  curHealth: number;
  atkStrategy: AtkStrategy;
  defStrategy: DefStrategy;
}

export interface AircraftInfo {
  id: number;
  amount: number;
  atkStrategy: AtkStrategy;
  defStrategy: DefStrategy;
}

export interface Damage {
  value: number;
  type: DamageType;
}
