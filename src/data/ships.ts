import { Maker } from './common';

enum ShipClass {
  Frigate,
  Destroyer,
  Cruiser,
}

export interface Ship {
  label: string;
  desc: string;
  class: ShipClass;
  maker: Maker;
  maxMods: (level: number) => number;
  maxSlots: number;
  size: (level: number) => number;
  health: (level: number) => number;
  shield: (level: number) => number;
  speed: (level: number) => number;
  resist: number[],
  buffs: {
    atk: (level: number) => number;
    def: (level: number) => number;
  };
  cargo: (level: number) => number;
  opCost: (level: number) => number;
}

export const ships: Ship[] = [
  {
    label: '破晓',
    desc: '多功能星舰',
    class: ShipClass.Frigate,
    maker: Maker.Union,
    maxMods: level => Math.floor(20 + level * 0.13),
    maxSlots: 5,
    size: level => Math.floor(20 + level * 2.3),
    health: level => Math.floor(1000 + level * 15),
    shield: () => 0,
    speed: () => 140,
    resist: [0.6, 1, 0],
    buffs: {
      atk: () => 1,
      def: () => 1,
    },
    cargo: level => Math.floor(50 + level * 2),
    opCost: level => Math.floor(100 + level * 12),
  },
];
