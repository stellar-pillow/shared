import { Cost } from './common';

const min: number = 60000;
const hour: number = 3600000;
const day: number = 86400000;

interface Struct {
  label: string;
  desc: string;
  inSpace: boolean;
  onLand: boolean;
  cost: (level: number) => Cost;
  power: (level: number) => number;
  duration?: (level: number) => number;
  incomePerH?: (level: number, buff: number) => number;
  limitBuff?: (level: number) => number;
}

const commonList: number[][] = [
  [0, 50, 10],
  [0, 100, 20],
  [1, 200, 32],
  [2, 500, 48],
  [4, 1000, 64],
  [6, 2000, 80],
  [10, 4000, 100],
  [15, 10000, 130],
  [20, 20000, 180],
  [25, 40000, 250],
];
const popList: number[] = [0, 0, 1, 2, 4, 6, 10, 15, 20, 25];
const timeList: number[] = [
  5000,
  2 * min,
  30 * min,
  1 * hour,
  2 * hour,
  8 * hour,
  1 * day,
  2 * day,
  3 * day,
  5 * day,
];
const resIdleList: number[] = [
  4 * hour,
  5 * hour,
  6 * hour,
  8 * hour,
  10 * hour,
  12 * hour,
  14 * hour,
  16 * hour,
  20 * hour,
  24 * hour,
];
const generatorList: number[][] = [
  [1, 80],
  [2, 100],
  [4, 200],
  [7, 400],
  [10, 2000],
  [15, 10000],
  [20, 30000],
  [25, 80000],
  [30, 150000],
  [40, 300000],
];
const gateList: number[][] = [
  [10, 8 * hour],
  [15, 7 * hour],
  [20, 6 * hour],
  [30, 5 * hour],
  [40, 4 * hour],
  [60, 3 * hour],
  [80, 2.5 * hour],
  [100, 2 * hour],
  [150, 1.5 * hour],
  [200, 1 * hour],
];

const generator: Struct = {
  label: '发电厂',
  desc: '为其他建筑提供电力',
  inSpace: true,
  onLand: true,
  cost: level => ({
    metal: generatorList[level - 1][1],
    crystal: generatorList[level - 1][1],
    pop: generatorList[level - 1][0],
    time: timeList[level - 1] * 2,
  }),
  power: () => 0,
  limitBuff: level => 2 ** level,
};

const metal: Struct = {
  label: '金属矿场',
  desc: '从星球中提取金属',
  inSpace: false,
  onLand: true,
  cost: level => ({
    metal: 10 * 2 ** 20,
    crystal: 5 * 2 ** 20,
    pop: popList[level - 1],
    time: timeList[level - 1],
  }),
  power: level => level,
  duration: level => resIdleList[level - 1],
  incomePerH: (level, buff) => 2.5 * level ** 2 + 10 * level * 8 * buff,
};

const crystal: Struct = {
  label: '水晶矿场',
  desc: '从星球中提取水晶',
  inSpace: false,
  onLand: true,
  cost: level => ({
    metal: 10 * 2 ** 20,
    crystal: 5 * 2 ** 20,
    pop: popList[level - 1],
    time: timeList[level - 1],
  }),
  power: level => level,
  duration: level => resIdleList[level - 1],
  incomePerH: (level, buff) => 2.5 * level ** 2 + 10 * level * 8 * buff,
};

const shelter: Struct = {
  label: '庇护所',
  desc: '生产人口并提高人口上限',
  inSpace: false,
  onLand: true,
  cost: level => ({
    metal: 15 * 2 ** 20,
    crystal: 5 * 2 ** 20,
    pop: 0,
    time: timeList[level - 1],
  }),
  power: level => level,
  duration: level => resIdleList[level - 1] * 2,
  incomePerH: (_level, buff) => 1 * buff,
  limitBuff: level => 10 * level,
};

const shipyard: Struct = {
  label: '造船厂',
  desc: '生产舰队',
  inSpace: true,
  onLand: true,
  cost: level => ({
    metal: commonList[level - 1][1] * 25,
    crystal: commonList[level - 1][1] * 10,
    pop: commonList[level - 1][0] * 2 + 1,
    time: timeList[level - 1] * 3,
  }),
  power: level => level * 2,
};

const scanner: Struct = {
  label: '扫描阵列',
  desc: '解锁探险任务',
  inSpace: true,
  onLand: true,
  cost: level => ({
    metal: commonList[level - 1][1] * 15,
    crystal: commonList[level - 1][1] * 15,
    pop: commonList[level - 1][0],
    time: timeList[level - 1] * 3,
  }),
  power: level => level * 2,
};

const spaceport: Struct = {
  label: '航空港',
  desc: '增加太空槽位',
  inSpace: false,
  onLand: true,
  cost: level => ({
    metal: 50 * 40 ** level,
    crystal: 60 ** level,
    pop: 2 ** level,
    time: day * 2 ** level,
  }),
  power: level => 2 ** level,
  limitBuff: level => level,
};

const gate: Struct = {
  label: '星门',
  desc: '在星球之间快速移动',
  inSpace: true,
  onLand: false,
  cost: level => ({
    metal: commonList[level - 1][1] * 20,
    crystal: commonList[level - 1][1] * 50,
    pop: (commonList[level - 1][0] + 1) * 3,
    time: timeList[level - 1] * 10,
  }),
  power: level => gateList[level - 1][0],
  duration: level => gateList[level - 1][1],
};

export enum StructId {
  Generator,
  Metal,
  Crystal,
  Shelter,
  Shipyard,
  Scanner,
  Spaceport,
  Gate,
}

export const structs: Struct[] = [
  generator,
  metal,
  crystal,
  shelter,
  shipyard,
  scanner,
  spaceport,
  gate,
];
